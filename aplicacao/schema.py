from pyexpat import model
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import graphene
from graphql_jwt.shortcuts import  get_token
from graphene_django import DjangoObjectType
from .models import (
    Clientes,
    AgendaServico,
    Caixa,
    Profissionais,
    Produto,
    Pagamento,
    Comissoes
)
 

class UserType(DjangoObjectType):
    class Meta:
        model = User


class AgendaServicoType(DjangoObjectType):

    class Meta:
        model=AgendaServico

 
class ClientesType(DjangoObjectType):

    class Meta:
        model=Clientes


class CaixaType(DjangoObjectType):

    class Meta:
        model=Caixa


class ProfissionaisType(DjangoObjectType):

    class Meta:
        model=Profissionais


class ProdutoType(DjangoObjectType):

    class Meta:
        model=Produto


class PagamentoType(DjangoObjectType):

    class Meta:
        model=Pagamento


class ComissoesType(DjangoObjectType):

    class Meta:
        model=Comissoes


class Query(graphene.ObjectType):

    cliente = graphene.Field(ClientesType, id=graphene.String())
    clientes = graphene.List(ClientesType)
    agendas = graphene.List(AgendaServicoType)
    caixas = graphene.List(CaixaType)
    proficionais = graphene.List(ProfissionaisType)
    produto = graphene.List(ProdutoType)
    pagamento = graphene.List(PagamentoType)
    comissoes = graphene.List(ComissoesType)

    def resolve_cliente(root,info,id):
        return Clientes.objects.get(id=id)
    
    def resolve_clientes(root,info):
        return Clientes.objects.all()

    def resolve_agendas(root,info):
        return AgendaServico.objects.all()

    def resolve_acaixas(root,info):
        return Caixa.objects.all()

    def resolve_proficionais(root,info):
        return Profissionais.objects.all()

    def resolve_produto(root,info):
        return Produto.objects.all()

    def resolve_pagamento(root,info):
        return Pagamento.objects.all()

    def resolve_comissoes(root,info):
        return Comissoes.objects.all()

class LoginMobile(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    token = graphene.String()
    user = graphene.Field(UserType)

    @classmethod
    def mutate(cls, root, info, username, password):
        user = User.objects.get(username=username)
        if user.check_password(password):
            # print(user)
            token = get_token(user)
            # print("token ",token)
        else:
            return ValidationError("Credenciais de acesso inválidos")
        return LoginMobile(token=token, user=user)


class Mutation(graphene.ObjectType):
    login = LoginMobile.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)