from django.contrib import admin
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
import debug_toolbar
# from django.shortcuts import render

urlpatterns = [
    path("__debug__/", include(debug_toolbar.urls)),
    # path('dynamic_raw_id/', include('dynamic_raw_id.urls')),
    path("", admin.site.urls),
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
]
